import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2

ApplicationWindow {
    title: "py2048-qml"
    visible: true
    maximumWidth: -1
    maximumHeight: -1
    ColumnLayout {
        id: grid
        focus: true
        anchors.fill: parent
        property var gameColours: ({
            0: "#eeeeee",
            2: "#7cb5e2",
            4: "#4495d4",
            8: "#2f6895",
            16: "#f5bd70",
            32: "#f2a032",
            64: "#cd8829",
            128: "#e37051",
            256: "#de5833",
            512: "#bd4a2b",
            1024: "#5454da",
            2048: "#3b3c99",
            4096: "#ffd700"
        })

        ColumnLayout {
            Repeater {
                model: game
                RowLayout {
                    property var row_index: index
                    Repeater {
                        model: game.columnCount()
                        Rectangle {
                            height: 64
                            width: 64
                            radius: height/8
                            property var d: game.data(game.index(parent.row_index,index))
                            color: d ? grid.gameColours[d] : grid.gameColours[0]
                            Text {
                                x: parent.width/2-contentWidth/2
                                y: parent.height/2-contentHeight/2
                                text: parent.d > 0 ? parent.d : ""
                                font.pixelSize: parent.height*0.4
                                font.bold: true
                                font.family: "Helvetica"
                            }
/*
                            Component.onCompleted: {
                                console.log('Y:'+parent.row_index)
                                console.log('X:'+index)
                                console.log('N:'+d)
                                console.log('C:'+grid.gameColours[d])
                            }
*/
                        }
                    }
                }
            }
        }

        Keys.onPressed: {
            if( [ Qt.Key_Left, Qt.Key_A, Qt.Key_H ].includes(event.key) ) {
                //console.log('left');
                game.shift(0);
                event.accepted = true;
            }
            if( [ Qt.Key_Right, Qt.Key_D, Qt.Key_L ].includes(event.key) ) {
                //console.log('right');
                game.shift(1);
                event.accepted = true;
            }
            if( [ Qt.Key_Up, Qt.Key_W, Qt.Key_K ].includes(event.key) ) {
                //console.log('up');
                game.shift(2);
                event.accepted = true;
            }
            if( [ Qt.Key_Down, Qt.Key_S, Qt.Key_J ].includes(event.key) ) {
                //console.log('down');
                game.shift(3);
                event.accepted = true;
            }
        }
    }
}
