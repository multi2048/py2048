#!/usr/bin/env python3
import lib2048

game = lib2048.lib2048()

print("Welcome to the game of 2048!")

turns = 0

while True:
    print("\n========================")
    print("Turns:",str(turns))
    turns += 1
    print("Score:", game.score)
    for row in game.board:
        print("+----"*len(game.board[0])+"+")
        print("|"+"|".join([str(c)+" "*(4-len(str(c))) if c>0 else " "*4 for c in row])+"|")
    print("+----"*len(game.board[0])+"+")
    if game.score==1:
        print("You got to 2048, congrats!")
    elif game.score==2:
        print("I can't find anything to combine, and no free space.")
        print("Seems you've lost this one. Sorry about that.")
    print("(L)eft, (U)p, (R)ight, (D)own, (Q)uit")
    inp = input("Command: ")
    if inp.lower() in ('q', 'quit'):
        break
    if inp.lower() in ('l','left'):
        game.shift_left()
        continue
    if inp.lower() in ('r','right'):
        game.shift_right()
        continue
    if inp.lower() in ('u','up'):
        game.shift_up()
        continue
    if inp.lower() in ('d','down'):
        game.shift_down()
        continue
    print("Invalid input.")
