#!/usr/bin/env python2
import curses
import lib2048

game = lib2048.lib2048()
def showgame():
    screen.addstr(2,7,str(game.score()))
    rs=len(game.board())
    cs=len(game.board()[0])
    if game.status()==1:
        screen.addstr(2,20,"CONGRATS!")
    elif game.status()==2:
        screen.addstr(2,20,"Sorry...")
    for r in range(rs):
        for c in range(cs):
            if game.board()[r][c] > 0:
                screen.addstr(4+(r*2),1+(c*5),str(game.board()[r][c])+' '*(4-len(str(game.board()[r][c]))))
            else:
                screen.addstr(4+(r*2),1+(c*5),' '*4)
screen = curses.initscr()
curses.noecho()
curses.cbreak()
curses.curs_set(0)
screen.keypad(True)
screen.addstr(2,0,'Score:')
screen.addstr(0,0,'Q: Quit')
for r in range(len(game.board())):
    screen.addstr(3+r*2,0, '+----'*len(game.board()[0])+'+')
    screen.addstr(4+r*2,0, '|    '*len(game.board()[0])+'|')
screen.addstr(3+len(game.board())*2,0, '+----'*len(game.board()[0])+'+')
showgame()
try:
    while True:
        char=screen.getch()
        if char == ord('q'):
            break
        elif char in [curses.KEY_RIGHT, ord('d'), ord('l')]:
            game.shift_right()
        elif char in [curses.KEY_LEFT, ord('a'), ord('h')]:
            game.shift_left()
        elif char in [curses.KEY_UP, ord('w'), ord('k')]:
            game.shift_up()
        elif char in [curses.KEY_DOWN, ord('s'), ord('j')]:
            game.shift_down()
        showgame()
finally:
    curses.nocbreak()
    screen.keypad(0)
    curses.echo()
    curses.endwin()
