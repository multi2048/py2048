#!/usr/bin/env python2

import random
import copy

class lib2048:
    def __init__(self, size=None):
        # size is list/tuple/etc of two ints
        if not size:
            size = (4,4)
        self._board = [ list(r) for r in [[0]*size[0]]*size[1] ]
        self._score = 0
        # Numbers to pick from when placing one, 90% chance of 2, 10% chance of 4
        self._num_choices = [2]*9+[4]
        for r in range(2):
            self._add_num()

    def score(self):
        return self._score

    def board(self):
        return self._board

    def status(self):
        # 0 = everything fine, continue playing
        # 1 = reached 2048, game won
        # 2 = game lost (nothing can combine, and there are no empty spaces)
        temp = []
        for r in self.board():
            temp += r
        if len([ w for w in temp if w>=2048])>=1:
            # Reached 2048, game is won
            return 1
        if 0 in temp:
            # There are empty spaces to add another number
            return 0
        for y in range(len(self.board())-1):
            for x in range(len(self.board()[0])):
                if self.board()[y][x] == self.board()[y+1][x]:
                    # Can combine vertically
                    return 0
        for y in range(len(self.board())):
            for x in range(len(self.board()[0])-1):
                if self.board()[y][x] == self.board()[y][x+1]:
                    # Can combine horizontally
                    return 0
        # No empty spaces, can't combine, and hasn't reached 2048. Game is lost.
        return 2

    def __repr__(self):
        # (accidentaly) JSON-compatible? representation
        # Probably useless
        return { 'score': self.score(), 'board': self.board(), 'status': [self.status(),['ok','won','lost'][self.status()]] }

    def _add_num(self):
        # Adds another 2 in a random free location on the board
        z=[]
        # Look up all positions with a 0
        for y in range(len(self.board())):
            for x in range(len(self.board()[0])):
                if not self.board()[y][x]:
                    z.append((x, y))
        if len(z):
            # Randomly pick one of the positions and set it to one of the numbers in _num_choices
            x,y = random.choice(z)
            self._board[y][x] = random.choice(self._num_choices)

    def _invert(self):
        # Switches x and y axis on the board
        # Used for the up/down-shifters (since right, up, and down goes through left)
        temp = [ list(y) for y in [[0]*len(self.board())]*len(self.board()[0]) ]
        for y in range(len(self.board())):
            for x in range(len(self.board()[0])):
                temp[x][y] = self.board()[y][x]
        self._board = temp

    def _combined(self,r):
        # Joins neighboring equal elements in a list to one with twice the value
        x=0
        while True:
            if x >= len(r)-1:
                break
            if r[x] == r[x+1]:
                r[x] *= 2
                self._score += r[x]
                r.remove(r[x+1])
            x += 1
        return r

    def shift_left(self):
        # Shifts all non-zero numbers as far left as they can go,
        # also used by all other shifters
        l = len(self.board()[0])
        orig_board = copy.deepcopy(self.board())
        for y in range(len(self._board)):
            while 0 in self._board[y]:
                self._board[y].remove(0)
            self._board[y] = self._combined(self._board[y])
            self._board[y] += [0]*(l-len(self._board[y])) # Make sure the row is as long as it should be
        if not orig_board == self.board(): # Only add another number if the board has changed
            self._add_num()

    def shift_right(self):
        for r in range(len(self._board)):
            self._board[r].reverse()
        self.shift_left()
        for r in range(len(self._board)):
            self._board[r].reverse()

    def shift_up(self):
        self._invert()
        self.shift_left()
        self._invert()

    def shift_down(self):
        self._invert()
        self.shift_right()
        self._invert()
